# dianalyse
Dianalyse is an analysis program for my diabetes data. It was initially a project for my final PHYS30762 Object Oriented Programming in C++ assignment, but I intend to keep working on it. The report explaining how the program works can be found on the project page on my website: http://goodbaddiabetes.com/dianalyse/

Included is a Makefile `make` which will compile the program. So far, only the g++ and the default Microsoft Visual Studio 2012 compilers have been tested (need c++11 flags in compilation - included in the Makefile) so there is no guarantee others will work.


There are 3 data files provied. faildata.csv, testdata.csv and largedata.csv.

 * faildata is a file which is deliberately designed to cause the program to demonstrate errors, for example bad data entries.
 * testdata is a file which only contains two days worth of data, for testing purposes.
 * largedata is a file which has 1 full months worth of data.
