// header file for the "menu" functions
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

#include "reading.h"// need to use the class
#include "analysis.h"// menu functions call the analysis functions

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

void introduction();
void main_menu();
bool check_key(int date, const DATA_MAP& m);
void bg_menu();
void bolus_menu();
void printing_menu();
void respond_main(char C, bool& r, const DATA_MAP& BG, const DATA_MAP& BOLUS);
void respond_bg_menu(char C, const DATA_MAP& BG);
void respond_bolus_menu(char C, const DATA_MAP& BOLUS);
void respond_printing_menu(char C, const DATA_MAP& BG, const DATA_MAP& BOLUS);

#endif