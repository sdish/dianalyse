// header file for the "analysis" functions
#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

#include "reading.h"// need to use the class

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

namespace daily {// namespace for all functions which perform analysis on a particular days readings
	double get_bg_average(const vector<ptr>& v);
	double get_bg_sd(const vector<ptr>& v);
	void print_bg(const vector<ptr>& v);
	void print_bolus(const vector<ptr>& v);
	double get_bolus_total(const vector<ptr>& v);
};

namespace monthly {// namespace for all functions which perform analysis on the entire months readings
	double get_bg_average(const DATA_MAP& m);
	double get_bg_sd(const DATA_MAP& m);
	double get_bolus_total(const DATA_MAP& m);
}

#endif 