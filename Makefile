# compiler options
CC=g++
CFLAGS=-std=c++11

# source files
SRC=src/*.cpp

all: dianalyse

dianalyse: main.o functions.o reading.o file_reading.o analysis.o
	$(CC) $(CFLAGS) $(SRC) -o dianalyse

main.o: src/main.cpp
	$(CC) $(CFLAGS) -c src/main.cpp

functions.o: src/functions.cpp
	$(CC) $(CFLAGS) -c src/functions.cpp

reading.o: src/reading.cpp
	$(CC) $(CFLAGS) -c src/reading.cpp

file_reading.o: src/file_reading.cpp
	$(CC) $(CFLAGS) -c src/file_reading.cpp

analysis.o: src/analysis.cpp
	$(CC) $(CFLAGS) -c src/analysis.cpp

clean:
	rm dianalyse *o
